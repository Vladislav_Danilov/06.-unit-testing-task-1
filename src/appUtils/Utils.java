package appUtils;

public class Utils {

  public ResultConcat concatenateWords(String stringInputOne, String stringInputTwo) {
    if (stringInputOne == null || stringInputTwo == null) {
      return ResultConcat.NULLSTRING;
    } else if ((stringInputOne + stringInputTwo).matches("[\\s\\p{L}\\p{M}&&[^\\p{Alpha}]]+")) {
      return ResultConcat.NONLATIN;
    } else if ((stringInputOne + stringInputTwo).isEmpty()) {
      return ResultConcat.EMPTYSTRING;
    } else {
      return ResultConcat.VERYSTRING;
    }
  }

  public int computeFactorial(int n) {
    if (n == 0)
      return 1;
    return n * computeFactorial(n - 1);
  }
}
