package test;

import static org.junit.Assert.*;

import java.util.Random;
import java.util.concurrent.Callable;

import org.junit.Ignore;
import org.junit.Test;

import appUtils.ResultConcat;
import appUtils.Utils;

public class UtilsTest {

  @Test
  public void testChecksConcatenateWordsOnNulls() {
    Utils tester = new Utils();
    assertEquals("Concatenate strings returned null", ResultConcat.NULLSTRING,
        tester.concatenateWords("", null));
  }

  @Test
  public void testChecksConcatenateWordsOnNonLatin() {
    Utils tester = new Utils();
    assertEquals("Concatenate strings returned non-latin", ResultConcat.NONLATIN,
        tester.concatenateWords("������", "������"));
  }

  @Test
  public void testChecksConcatenateWordsOnEmptyString() {
    Utils tester = new Utils();
    assertEquals("Concatenate strings returned emptyString", ResultConcat.EMPTYSTRING,
        tester.concatenateWords("", ""));
  }

  @Test(timeout = 1000)
  public int testFactorialWithTimeou() {
    Utils tester = new Utils();
    Random random = new Random();
    return tester.computeFactorial(random.nextInt(100));
  }

  static class Task implements Callable<String> {
    @Override
    public String call() throws Exception {
      Thread.sleep(4000); // Just to demo a long running task of 4 seconds.
      return "Ready!";
    }
  }

}
